#marathon
setwd("C:/����� ���/datafiles")
house.original <- read.csv("kc_house_data.csv")
house <- house.original

str(house)
summary(house)
head(house)

str(as.factor(house$id))
house$id <- NULL
house$date <- as.character(house$date)

#DAte - year , month
?substr
house$year <- substr(house$date,1,4)
house$month <- substr(house$date,5,6)
summary(as.factor(house$month))
table(house$month)
table(house$year)
house.year <- as.factor(house$year)

#bining (roni code
#breaks <- c(0,7,8,9,17,20,21,22,24)
#lables <- c("0-6", "7", "8", "9-16", "17-19", "20", "21", "22-23")
#bins <- cut(bikes.data.prepared$hour, breaks, include.lowest = TRUE, right = FALSE, labels = lables)

#binning
house$month <- as.numeric(house$month)

breaks <- c(0,7,12)
bins <- cut(house$month, breaks, include.lowest = T,right = F, labels = c('Half1','Half2'))
table(bins)
house$half <- bins
str(house)
house$month <- NULL

summary(house)

yeRevontedDeal <- function(x){
  if(x == 0){
    return('not revonted')
  }
  else{
    return('revonted')
  }
}

house$yr_renovated <- sapply(house$yr_renovated, yeRevontedDeal)
house$yr_renovated <- as.factor(house$yr_renovated)
str(house$yr_renovated)

#eda
library(ggplot2)
house$date <- NULL
ggplot(house, aes(price)) + geom_histogram()
#Bedroom
ggplot(house, aes(bedrooms, price)) + geom_point() + stat_smooth(method = lm)

#effect a bit
corrctBedrooms <- function(x){
  return(ifelse(x>10, 10, x))
}
house$bedrooms <- sapply(house$bedrooms, corrctBedrooms )
table(house$bedrooms)

#factor affect number - yr_revonted affect price

ggplot(house, aes(yr_renovated,price)) + geom_boxplot()
#affecting

library(caTools)

filter <- sample.split(house$price, SplitRatio = 0.7) #its not matter which field you use

#training set
house.train <- subset(house, filter ==TRUE)

#test set
house.test <- subset(house, filter ==F)

dim(house)
dim(house.train)
dim(house.test)

model <- lm(price ~ .,house.train)

summary(model)

predict.train <- predict(model,house.train)
predict.test <- predict(model,house.test)


#MSE overfiting and underfitting
mse.train <- mean((house.train$price - predict.train)**2)
#if overfitting ->namoch
mse.test <- mean((house.test$price - predict.test)**2)

me.train <- mse.train**0.5
me.test <- mse.test**0.5

precent.train <- me.train/mean(house$price)
precent.test <- me.test/mean(house$price)
#underfitting
