setwd("C:/����� ���/datafiles")
bikes.data.raw <- read.csv("bike sharing.csv")

str(bikes.data.raw)
library(ggplot2)
bikes.data.prepared <- bikes.data.raw


#turn date time into character
bikes.data.prepared$datetime <-  as.character(bikes.data.prepared$datetime)
str(bikes.data.prepared)


Sys.setlocale("LC_TIME", "English")
date <- substr(bikes.data.prepared$datetime, 1,10)
days <- weekdays(as.Date(date))

#Add weekdays
bikes.data.prepared$weekday <- as.factor(days)

str(bikes.data.prepared)

#Add month
month <- as.integer(substr(date ,6,7))
bikes.data.prepared$month <- month

#Add year
year <- as.integer(substr(date ,1,4))
bikes.data.prepared$year <- year

#Add hour
hour <- as.integer(substr(bikes.data.prepared$datetime ,12,13))
bikes.data.prepared$hour <- hour

#Find seasons
table(bikes.data.prepared$season,bikes.data.prepared$month)

#Turn season into factor
bikes.data.prepared$season <- factor(bikes.data.prepared$season, levels = 1:4, labels = c('winter','spring', 'summer', 'automn' ))

#Find weather                                                                                                                                                                                    'spring', 'summer', 'automn' ))
table(bikes.data.prepared$weather,bikes.data.prepared$month)
bikes.data.prepared$weather <- factor(bikes.data.prepared$weather, levels = 1:4, labels = c('clear',
                                 'cloudy', 'light rain', 'heavy rain'))
str(bikes.data.prepared)                                      

#Boxplot the influance of the *season* on the amount of bike rentals
ggplot(bikes.data.prepared, aes(x=season, y=count, color = season)) +
  geom_boxplot(outlier.colour="red", outlier.shape=16,
               outlier.size=1, notch=FALSE) +
                 stat_summary(fun=mean, geom="point", shape=23, size=4)

#Boxplot the influance of the *weather* on the amount of bike rentals
ggplot(bikes.data.prepared, aes(x=weather, y=count, color = weather)) +
  geom_boxplot(outlier.colour="red", outlier.shape=16,
               outlier.size=1, notch=FALSE) +
  stat_summary(fun=mean, geom="point", shape=23, size=4)

#look at time impact
ggplot(bikes.data.prepared, aes(as.factor(year), count)) + geom_boxplot()
