setwd("C:/����� ���/datafiles")
shopping.raw <- read.csv('shopping.csv')
shopping <- shopping.raw


str(shopping)

#target balance 
library(ggplot2)

ggplot(shopping, aes(Revenue))+geom_bar()

ggplot(shopping, aes(Administrative,fill= Revenue))+geom_bar()
ggplot(shopping, aes(Administrative,fill= Revenue))+geom_bar(position = 'fill')

coercx <- function(x, by){
  if(x<=by){
    return(x)}
  return(by)
}

shopping$Administrative <- sapply(shopping$Administrative,coercx,by=10)


ggplot(shopping, aes(Informational,fill= Revenue))+geom_bar()
ggplot(shopping, aes(Informational,fill= Revenue))+geom_bar(position = 'fill')


ggplot(shopping, aes(PageValues,fill= Revenue))+geom_histogram()
ggplot(shopping, aes(PageValues,fill= Revenue))+geom_histogram(position = 'fill')

shopping$PageValues <- sapply(shopping$PageValues,coercx,by=50)

#install.packages('corrplot')
library(corrplot)

dur.df <- shopping[,c("Administrative_Duration", "Informational_Duration", "ProductRelated_Duration")]
str(dur.df)

corr.matrix<-cor(dur.df)
corrplot(corr.matrix, method = 'circle')


#install.packages('rpart')
library(rpart)
#install.packages('rpart.plot')
library(rpart.plot)

library(caTools)

filter <- sample.split(shopping$Revenue, SplitRatio = 0.7)
shopping.train <- subset(shopping, filter == T)
shopping.test <- subset(shopping, filter == F)


model.dt <- rpart(Revenue ~ ., shopping.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

prediction <- predict(model.dt,shopping.test)
actual <- shopping.test$Revenue

cf <- table(actual,prediction > 0.5)
precision <- cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['TRUE','FALSE'])
recall <- cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['FALSE','TRUE'])

#install.packages("randomForest")
library(randomForest)


model.rf <- randomForest(Revenue ~ ., data = shopping.train, importance = TRUE)

prediction.rf <- predict(model.rf,shopping.test)
actual <- shopping.test$Revenue

cf.rf <- table(actual,prediction.rf > 0.5)
precision <- cf.rf['TRUE','TRUE']/(cf.rf['TRUE','TRUE'] + cf.rf['TRUE','FALSE'])
recall <- cf.rf['TRUE','TRUE']/(cf.rf['TRUE','TRUE'] + cf.rf['FALSE','TRUE'])


#ROC curve 

#install.packages('pROC')
library(pROC)

rocCurveDC <- roc(actual,prediction, direction = ">", levels = c("TRUE", "FALSE"))
rocCurveRF <- roc(actual,prediction.rf, direction = ">", levels = c("TRUE", "FALSE"))

#plot the chart 
plot(rocCurveDC, col = 'red',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveRF, col = 'blue',main = "ROC Chart")

auc(rocCurveDC)
auc(rocCurveRF)






