setwd("C:/����� ���/datafiles")
library(ggplot2)

houses.raw <- read.csv("kc_house_data.csv")
houses.perpared <- houses.raw

str(houses.perpared)


summary(houses.perpared)

#removing non informative data
#id has no data 
houses.perpared$id <- NULL

#Date reflects just one year
#Not a wide enough range for real estate prices 
houses.perpared$date <- NULL 



#EDA - Exploratory data analysis 
#And feature engineering 

ggplot(houses.perpared, aes(price)) + geom_histogram(binwidth = 100000) 



#bedrooms
table(houses.perpared$bedrooms)

#Data needs coercing (liniting range)
#0->1
#8-33 ->7 

limit.bedrooms <- function(beds){
  if (beds == 0) return (1)
  if (beds > 7) return (7)
  return (beds)
}

houses.perpared$bedrooms <- sapply(houses.perpared$bedrooms,limit.bedrooms )

table(houses.perpared$bedrooms)

#See how bedrooms affects prices 

ggplot(houses.perpared, aes(bedrooms, price)) + geom_point() + stat_smooth(method = lm)

#bathrooms
table(houses.perpared$bathrooms)

#Data needs coercing (liniting range)
#X<1 ->1
#x>4.5 ->5

limit.bathrooms <- function(baths){
  if (baths < 1) return (1)
  if (baths > 4.5) return (5)
  return (baths)
}

houses.perpared$bathrooms <- sapply(houses.perpared$bathrooms,limit.bathrooms)

table(houses.perpared$bathrooms)

ggplot(houses.perpared, aes(bathrooms, price)) + geom_point() + stat_smooth(method = lm)


#sqft_living
ggplot(houses.perpared, aes(sqft_living, price)) + geom_point() + stat_smooth(method = lm)

#sqft_lot
ggplot(houses.perpared, aes(sqft_lot, price)) + geom_point() + stat_smooth(method = lm)

#floors 
table(houses.perpared$floors)

#Data needs coercing (liniting range)
#X>2.5 ->2.5

limit.floors <- function(floors){
  if (floors > 2.5) return (2.5)
  return (floors)
}

houses.perpared$floors <- sapply(houses.perpared$floors,limit.floors)

table(houses.perpared$floors)

ggplot(houses.perpared, aes(floors, price)) + geom_point() + stat_smooth(method = lm)

#waterfront
ggplot(houses.perpared, aes(as.factor(waterfront), price)) +geom_boxplot()

str(houses.perpared)

#view
table(houses.perpared$view)
ggplot(houses.perpared, aes(as.factor(view), price)) +geom_boxplot()

#condition
table(houses.perpared$condition)
ggplot(houses.perpared, aes(as.factor(condition), price)) +geom_boxplot()

#grade
table(houses.perpared$grade)

#Data needs coercing (liniting range)
#X<5 ->5

limit.grade <- function(grade){
  if (grade < 5) return (5)
  return (grade)
}

houses.perpared$grade <- sapply(houses.perpared$grade,limit.grade)


ggplot(houses.perpared, aes(as.factor(grade), price)) +geom_boxplot()

#sqft_above
ggplot(houses.perpared, aes(sqft_above, price)) + geom_point() + stat_smooth(method = lm)

#sqft_basement
ggplot(houses.perpared, aes(sqft_basement, price)) + geom_point() + stat_smooth(method = lm)

#yr_built
ggplot(houses.perpared, aes(yr_built, price)) + geom_point() + stat_smooth(method = lm)

ggplot(houses.perpared, aes(yr_built)) + geom_histogram()

#yr_renovated
ggplot(houses.perpared, aes(yr_renovated)) + geom_histogram()

table(houses.perpared$yr_renovated)
#before 2000 -> 0
#after 2000 -> 1

limit.renovated <- function(year){
  if (year < 2000) return (0)
  return (1)
}

houses.perpared$renovated <- sapply(houses.perpared$yr_renovated,limit.renovated)
table(houses.perpared$renovated)

ggplot(houses.perpared, aes(as.factor(renovated), price)) +geom_boxplot()

#remove the original property 
houses.perpared$yr_renovated <- NULL 

#zipcode
table(houses.perpared$zipcode)

ggplot(houses.perpared, aes(as.factor(zipcode), price)) +geom_boxplot()

#If we new more about neighbourhoods we could have combined zipcodes
houses.perpared$zipcode <- as.factor(houses.perpared$zipcode)


#lat 
ggplot(houses.perpared, aes(lat, price)) + geom_point() + stat_smooth(method = lm)

#long 
ggplot(houses.perpared, aes(long, price)) + geom_point() + stat_smooth(method = lm)

houses.perpared$long <- NULL

#sqft_living15
ggplot(houses.perpared, aes(sqft_living, sqft_living15)) + geom_point() + stat_smooth(method = lm)

#sqft_lot15
ggplot(houses.perpared, aes(sqft_lot, sqft_lot15)) + geom_point() + stat_smooth(method = lm)

houses.perpared$sqft_living15 <- NULL 
houses.perpared$sqft_lot15 <- NULL 


str(houses.perpared)

#Seprating into train and test 
library(caTools)

filter <- sample.split(houses.perpared$price, SplitRatio = 0.7)

#Training set 
houses.train <- subset(houses.perpared, filter==TRUE)

#test set 
houses.test <- subset(houses.perpared, filter==FALSE)

dim(houses.perpared)
dim(houses.train)
dim(houses.test)

dim(houses.train)/dim(houses.perpared)

model <- lm(price ~ .,houses.train )

summary(model)

predict.train <- predict(model,houses.train)
predict.test  <- predict(model,houses.test)

MSE.train <- mean((houses.train$price - predict.train)**2)
MSE.test <- mean((houses.test$price - predict.test)**2)

RMSE.train <- MSE.train**0.5
RMSE.test <- MSE.test**0.5

predict.all <- predict(model,houses.perpared)

houses.perpared$predict <- predict.all

ggplot() + 
  geom_line(data = houses.perpared, aes(sqft_living, price), color = 'red' ) +
  geom_line(data = houses.perpared, aes(sqft_living, predict), color = 'blue') 

ggplot(houses.perpared, aes(price, predict)) + geom_point() 
